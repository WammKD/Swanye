defmodule SwanyeWeb.UsersController do
  use    SwanyeWeb, :controller;
  import Kabare.Utils.ConfigUtils;
  alias  Swanye.Repo;
  alias  Kabare.URLs.URL;
  alias  Kabare.GenerateAttrs;
  alias  Kabare.Users;
  alias  Kabare.Utils.ActivityPubUtils;
  alias  Kabare.ActivityPubFetch;
  alias  Kabare.ActivityPubActors;
  alias  Kabare.ActivityPubObjects;
  alias  Kabare.ActivityPubActivities;

  def shared_inbox(conn, params) do
    case ActivityPubUtils.verify_signature(conn, "Shared") do
      %Plug.Conn{} = c  -> c;
      {:error, _reason} -> halt(send_resp(conn,
                                          401,
                                          "Request signature could not be verified"));
      {:ok,      false} -> halt(send_resp(conn,
                                          401,
                                          "Request signature could not be verified"));
      {:ok,       true} ->
        IO.puts(IO.ANSI.yellow() <> "\n\n\n\nPISSERFUCK, Part 2\n\n\n\n");
        IO.puts(IO.ANSI.yellow() <> inspect(params, pretty: true, limit: :infinity))
        piss = GenerateAttrs.generate_activity_attrs(params, nil);
        IO.puts(IO.ANSI.yellow() <> "\n\n\n\nPISSERFUCK\n\n\n\n");
        IO.puts(IO.ANSI.yellow() <> inspect(piss, pretty: true, limit: :infinity))
        case ActivityPubActivities.create_activity(piss)    do
          {:error, _changeset} -> halt(send_resp(conn,
                                                 401,
                                                 "Request signature could not be verified"));
          {:ok,        _entry} -> halt(send_resp(conn, 200, "OK"));
        end;
    end
  end
  def inbox(conn, params) do
    IO.puts(IO.ANSI.yellow() <> inspect("LISTEN HERE", pretty: true, limit: :infinity));
    IO.puts(IO.ANSI.yellow() <> inspect(conn, pretty: true, limit: :infinity));
    case Users.get_user_by_username(params["username"]) do
      nil  -> halt(send_resp(conn, 404, "User does not exist."));
      user ->
        case ActivityPubUtils.verify_signature(conn, "#{params["username"]}'s") do
          %Plug.Conn{} = c  -> c;
          {:error, _reason} -> halt(send_resp(conn,
                                              401,
                                              "Request signature could not be verified"));
          {:ok,      false} -> halt(send_resp(conn,
                                              401,
                                              "Request signature could not be verified"));
          {:ok,       true} ->
            case ActivityPubActivities.create_activity(GenerateAttrs.generate_activity_attrs(params,
                                                                                             user)) do
              {:error, _changeset} -> halt(send_resp(conn,
                                                     401,
                                                     "Request signature could not be verified"));
              {:ok,        _entry} -> halt(send_resp(conn, 200, "OK"));
            end;
        end;
    end
  end

  def profile(conn, %{"actor" => actor}) do
    case String.split(actor, "@") do
      [name]           -> local_account(conn,
                                        fn() -> case name |> ActivityPubActors.get_actor_by_pref_name()
                                                          |> ActivityPubActors.preload_extras()         do
                                                  nil -> nil;
                                                  a   -> {a, ActivityPubActivities.get_activities_by_actor(a)};
                                                end;                                              end,
                                        fn() -> Users.get_user_by_username(name); end,
                                        "profile.html");
      [name,
       domain]         -> remote_account(conn,
                                         domain,
                                         fn() ->
                                           actor = name |> ActivityPubFetch.fetch_actor(domain)
                                                        |> ActivityPubActors.preload_extras();

                                           {actor, ActivityPubActivities.get_activities_by_actor(actor)};
                                         end,
                                         &(&1.object.activity_pub_id),
                                         [name],
                                         "profile.html");
      [_name,
      _domain | _tail] -> halt(send_resp(conn, 404, "User does not exist."));
    end
  end

  def followers(conn, %{"actor" => actor} = _params) do
    case String.split(actor, "@") do
      [name]           -> local_account(conn,
                                        fn() -> case name |> ActivityPubActors.get_actor_by_pref_name()
                                                          |> ActivityPubActors.preload_extras()         do
                                                  nil -> nil;
                                                  a   -> {a, nil};
                                                end;                                              end,
                                        fn() -> Repo.preload(Users.get_user_by_username(name),
                                                             [actor: :followers]).actor.followers; end,
                                        "followers.html");
      [name,
       domain]         -> remote_account(conn,
                                         domain,
                                         fn() -> {ActivityPubActors.preload_extras(ActivityPubActors.get_actor_by_pref_name(name,
                                                                                                                            domain)), nil}; end,
                                         &(&1.object.activity_pub_id),
                                         [name],
                                         "followers.html");
      [_name,
       _domain | _tail] -> halt(send_resp(conn, 404, "User does not exist."));
    end
  end

  def following(conn, %{"actor" => actor}) do
    case String.split(actor, "@") do
      [name]           -> local_account(conn,
                                        fn() -> case name |> ActivityPubActors.get_actor_by_pref_name()
                                                          |> ActivityPubActors.preload_extras()         do
                                                  nil -> nil;
                                                  a   -> {a, nil};
                                                end;                                              end,
                                        fn() -> Repo.preload(Users.get_user_by_username(name),
                                                             [actor: :following]).actor.following; end,
                                        "following.html");
      [name,
       domain]         -> remote_account(conn,
                                         domain,
                                         fn() -> {ActivityPubActors.preload_extras(ActivityPubActors.get_actor_by_pref_name(name,
                                                                                                                            domain)), nil}; end,
                                         &(&1.object.activity_pub_id),
                                         [name],
                                         "following.html");
      [_name,
       _domain | _tail] -> halt(send_resp(conn, 404, "User does not exist."));
    end
  end

  def post(conn, %{"postID" => postID, "actor" => actor}) do
    case String.split(actor, "@") do
      [name]           -> local_account(conn,
                                        fn() -> {nil,
                                                 case ActivityPubActivities.get_activity_by_objects_url_id_and_username(String.to_integer(postID),
                                                                                                                        name)                      do
                                                   nil -> case ActivityPubObjects.get_object_by_url_id_and_username(String.to_integer(postID),
                                                                                                                    name)                      do
                                                            nil -> nil;
                                                            obj -> %{actors:            obj.attributees,
                                                                     acted_upon_object: obj};
                                                          end;
                                                   act -> act;
                                                 end}; end,
                                        fn() -> ActivityPubObjects.get_object_by_url_id_and_username(postID,
                                                                                                     name);   end,
                                        "post.html");
      [name,
       domain]         -> remote_account(conn,
                                         domain,
                                         fn() -> {nil,
                                                  case ActivityPubActivities.get_activity_by_objects_url_id_and_username(String.to_integer(postID),
                                                                                                                         name,
                                                                                                                         domain)                    do
                                                    nil -> case ActivityPubObjects.get_object_by_url_id_and_username(String.to_integer(postID),
                                                                                                                     name,
                                                                                                                     domain)                    do
                                                             nil -> nil;
                                                             obj -> ActivityPubActivities.get_obj_parent_posts_recursively([obj.db_id])
                                                                    |> Enum.map(&(%{actors:            &1.attributees,
                                                                                    object:            %{ap_id:       "#{&1.ap_id}_create",
                                                                                                         object_type: "Create"},
                                                                                    acted_upon_object: &1}))
                                                                    |> List.first();
                                                           end;
                                                    act -> act;
                                                  end}; end,
                                         &(&1.activity_pub_id),
                                         [name, postID],
                                         "post.html");
      [_name,
       _domain | _tail] -> halt(send_resp(conn, 404, "User does not exist."));
    end
  end

  defp remote_account(conn,             domain,         entityFunct,
                      redirectURLfunct, fallbackParams, renderPage) do
    controllerFunctionAtom = String.to_atom(String.replace(renderPage,
                                                           ~r/\.html$/,
                                                           ""));

    if(instance_authority() === domain) do
      Phoenix.Controller.redirect(conn,
                                  to: apply(Routes,
                                            :users_path,
                                            [conn,
                                             controllerFunctionAtom] ++ fallbackParams));
    else
      {actor, posts} = entityFunct.();

      case Plug.Conn.get_session(conn, "current_user") do
        nil -> Phoenix.Controller.redirect(conn, external: URL.to_string(redirectURLfunct.(actor)));
        id  -> render(conn,
                      renderPage,
                      viewedActor:    actor,
                      postActivities: posts,
                      loggedInUser:   Users.get_full_with_likes_and_followees!(id),
                      page:           controllerFunctionAtom);
      end
    end
  end
  defp local_account(conn, entityFunct, activityStreamFunct, renderPage) do
    if(ActivityPubUtils.activity_stream?(conn)) do
      case activityStreamFunct.() do
        nil           -> render(conn, "404.html");
        entityForJSON -> Phoenix.Controller.json(conn, entityForJSON);
      end
    else
      case entityFunct.() do
        nil            -> render(conn, "404.html");
        {actor, posts} -> render(conn,
                                 renderPage,
                                 viewedActor:    actor,
                                 postActivities: posts,
                                 loggedInUser:   case Plug.Conn.get_session(conn, "current_user") do
                                                   nil -> nil;
                                                   id  -> Users.get_full_with_likes_and_followees!(id);
                                                 end,
                                 page:           String.to_atom(String.replace(renderPage,
                                                                               ~r/\.html$/,
                                                                               "")));
      end
    end
  end
end
