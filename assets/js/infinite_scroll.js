import activateMDE from "./activate-easy-markdown-editor";

const scrollAt = function() {
                   const scrollTop    = document.documentElement.scrollTop    || document.body.scrollTop;
                   const scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
                   const clientHeight = document.documentElement.clientHeight;

                   return scrollTop / (scrollHeight - clientHeight) * 100;
                 };

console.log("WHAT THE HELL?");
InfiniteScroll = { page()        { return this.el.dataset.page; },
                   mounted()     {
                     console.log("WHAT THE FUCK?");
                     this.pending = this.page();

                     window.addEventListener("scroll",
                                             (e) => {
                                               if(this.pending == this.page() &&
                                                  scrollAt()   >  90) {
                                                 this.pending = this.page() + 1;
                                                 document.querySelector(".dash_main > .spinner")
                                                         .style
                                                         .display                                = "inherit";

                                                 this.pushEvent("load-more-posts", {});
                                               }
                                             });
                   },
                   reconnected() { this.pending = this.page(); },
                   updated()     {
                     document.querySelector(".dash_main > .spinner")
                             .style
                             .display                                = "none";

                     if(typeof activateMDE === "function") {
                       document.querySelectorAll("form textarea[name=modal_editor]")
                               .forEach(function(textarea) {
                                          if(textarea.nextElementSibling
                                                     .getAttribute("class") !== "EasyMDEContainer") {
                                            activateMDE(textarea);
                                          }
                                        });
                     }

                     this.pending = this.page();
                   } };

export default InfiniteScroll;
