defmodule SwanyeWeb.PageController do
  use   SwanyeWeb, :controller;
  alias Kabare.Users;

  def index(conn, params) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> page_num = case params do
                          %{"page" => pg_num} -> case Integer.parse(pg_num) do
                                                   :error   -> 1;
                                                   {num, _} -> num;
                                                 end
                          _                   -> 1;
                        end;
             size_num = case params do
                          %{"size" => sz_num} -> case Integer.parse(sz_num) do
                                                   :error   -> 20;
                                                   {num, _} -> num;
                                                 end
                          _                   -> 20;
                        end;

             render(conn,
                    "index.html",
                    user:      Users.get_full_user_with_timeline_posts!(id,
                                                                        page_num,
                                                                        size_num),
                    page_type: :dash,
                    page_num:  page_num);
    end
  end
end
