defmodule Swanye.Repo do
  use Ecto.Repo,
    otp_app: :swanye,
    adapter: Ecto.Adapters.MyXQL
end
