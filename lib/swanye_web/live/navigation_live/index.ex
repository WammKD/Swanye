defmodule SwanyeWeb.NavigationLive.Index do
  use SwanyeWeb, :live_view;

  @impl true
  def mount(_params, session, socket) do
    userID = session["user_id"];

    if(userID > 0 and connected?(socket)) do
      SwanyeWeb.Endpoint.subscribe("live:notifications_and_counts_#{userID}");
    end

    {:ok, assign(socket, user_id:            userID,
                         notification_count: session["notification_count"],
                         new_posts_count:    session["new_posts_count"],
                         page_type:          session["page_type"])};
  end

  @impl true
  def handle_info(%Phoenix.Socket.Broadcast{event:   "notification",
                                            payload: %{"add" => add?}}, socket) do
    count = socket.assigns.notification_count;

    {:noreply, assign(socket,
                      notification_count: (if add?, do: count + 1, else: count - 1))};
  end

  @impl true
  def handle_info(%Phoenix.Socket.Broadcast{event:   "timeline",
                                            payload: %{"add" => add?}}, socket) do
    count = socket.assigns.new_posts_count;

    {:noreply, assign(socket,
                      new_posts_count: (if add?, do: count + 1, else: count - 1))};
  end
end
