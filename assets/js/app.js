// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
/* import "../css/app.css" */

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html";
import {Socket}       from "phoenix";
import {LiveSocket}   from "phoenix_live_view";
import InfiniteScroll from "./infinite_scroll";
/* import topbar from "../vendor/topbar"; */

console.log("WHAT THE SHIT?");
const Hooks      = { InfiniteScroll };
const csrfToken  = document.getElementById("meta-csrf").getAttribute("content");
const liveSocket = new LiveSocket("/live",
                                  Socket,
                                  { hooks:   Hooks,
                                    params:  { _csrf_token: csrfToken },
                                    timeout: 50000                      });

// Show progress bar on live navigation and form submits
/* topbar.config({barColors: {0: "#29d"}, shadowColor: "rgba(0, 0, 0, .3)"});
 * window.addEventListener("phx:page-loading-start", info => topbar.show());
 * window.addEventListener("phx:page-loading-stop", info => topbar.hide()); */

console.log("WHAT THE PISS?");
// connect if there are any LiveViews on the page
liveSocket.connect();
console.log("WHAT THE DAMN?");
liveSocket.enableDebug();

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket;
