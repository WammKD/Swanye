defmodule SwanyeWeb.RoomChannel do
  use Phoenix.Channel;

  def join("room:notifications", _message, socket) do
    {:ok, socket}
  end

  def handle_in("new_msg:" <> userID, %{"id"     => id,
                                        "add"    => add,
                                        "type"   => type,
                                        "actors" => actors}, socket) do
    broadcast!(socket, "new_msg:#{userID}", %{id:   id,   add:    add,
                                              type: type, actors: actors});

    {:noreply, socket}
  end
end
