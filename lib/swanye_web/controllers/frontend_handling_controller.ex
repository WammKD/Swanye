defmodule SwanyeWeb.FrontendHandlingController do
  use   SwanyeWeb, :controller;
  alias Swanye.Repo;
  alias Kabare.URLs;
  alias Kabare.URLs.URL;
  alias Kabare.Users;
  alias Kabare.Utils.DatabaseUtils;
  alias Kabare.Utils.ActivityPubUtils;
  alias Kabare.ActivityPubFetch;
  alias Kabare.ActivityPubActors;
  alias Kabare.ActivityPubActivities;

  def search_by_partial_name(conn, %{"search" => search}) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> actors = if(search === "") do
                        u = Repo.preload(Users.get_user!(id),
                                         [followees: [object: [images: [object: [url: [:origin,
                                                                                       :path]]]]]]);

                        if(Enum.empty?(u.followees)) do
                          Repo.preload(u,
                                       [followers: [object: [images: [object: [url: [:origin,
                                                                                     :path]]]]]]).followers;
                        else
                          u.followees;
                        end
                      else
                        ActivityPubActors.get_actors_by_partial_name(search);
                      end;

             send_resp(conn,
                       200,
                       Jason.encode!(Enum.map(actors,
                                              &(%{handle: ActivityPubUtils.activity_pub_handle(&1,
                                                                                               true),
                                                  icon:   case Enum.filter(&1.object.images,
                                                                           fn(img) -> img.is_icon; end) do
                                                            []             -> "/images/missing.png";
                                                            [head | _tail] -> URL.to_string(head.object.url);
                                                          end,
                                                  name:   &1.object.name}))));
    end
  end

  def like(conn, %{"inbox" => inbox, "objectID" => objectID, "action" => action}) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user   = Users.get_full_user!(id);
             userID = URL.to_string(user.actor.object.activity_pub_id);

             case action do
                 "do" -> ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    &(userID <> "/liked/" <> &1),
                                                    %{"@context" => "https://www.w3.org/ns/activitystreams",
                                                      "type"     => "Like",
                                                      "actor"    => userID,
                                                      "object"   => objectID});
               "undo" -> actURL = URL.to_string(List.first(ActivityPubActivities.get_activity_by_type_object_and_actors("Like",
                                                                                                                        objectID,
                                                                                                                        [userID]))
                                                    .object
                                                    .activity_pub_id);

                         ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    fn(_num) -> actURL <> "/undo"; end,
                                                    %{"@context" => "https://www.w3.org/ns/activitystreams",
                                                      "type"     => "Undo",
                                                      "actor"    => userID,
                                                      "object"   => actURL});
             end

             send_resp(conn, 200, "OK");
    end
  end

  def reblog(conn, %{"inbox"    => inbox,    "objectID" => objectID,
                     "username" => username,  "actorID" =>  actorID, "action" => action}) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user   = Users.get_full_user!(id);
             userID = DatabaseUtils.to_ap_id(user.actor);

             case action do
                 "do" -> ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    &(userID <> "/reblogged/" <> &1),
                                                    %{"@context"  => "https://www.w3.org/ns/activitystreams",
                                                      "type"      => "Announce",
                                                      "to"        => ["https://www.w3.org/ns/activitystreams#Public"],
                                                      "cc"        => [actorID, DatabaseUtils.to_ap_id(user.actor.followers)],
                                                      "actor"     => userID,
                                                      "object"    => objectID,
                                                      "username"  => username,
                                                      "published" => Calendar.strftime(DateTime.utc_now(),
                                                                                       "%Y-%m-%dT%H:%M:%SZ")});
               "undo" -> actURL = URL.to_string(List.first(ActivityPubActivities.get_activity_by_type_object_and_actors("Announce",
                                                                                                                        objectID,
                                                                                                                        [userID]))
                                                    .object
                                                    .activity_pub_id);

                         ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    fn(_num) -> actURL <> "/undo"; end,
                                                    %{"@context" => "https://www.w3.org/ns/activitystreams",
                                                      "type"     => "Undo",
                                                      "actor"    => userID,
                                                      "object"   => actURL,
                                                      "to"       => ["https://www.w3.org/ns/activitystreams#Public"]});
             end

             send_resp(conn, 200, "OK");
    end
  end

  def delete(conn, %{"objectID" => objectID}) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user   = id |> Users.get_full_user!()
                         |> Repo.preload([:followers, :followees]);
             userID = URL.to_string(user.actor.object.activity_pub_id);

             ActivityPubUtils.send_json(user,
                                        (user.followers ++
                                         user.followees) |> Enum.map(&(case &1.shared_inbox_id do
                                                                         nil -> &1.inbox_id;
                                                                         num -> num;
                                                                       end))
                                                         |> URLs.get_urls()
                                                         |> Enum.map(&URL.to_string/1),
                                        &(userID <> "/deleted/" <> &1),
                                        %{"@context"  => "https://www.w3.org/ns/activitystreams",
                                          "type"      => "Delete",
                                          "to"        => ["https://www.w3.org/ns/activitystreams#Public"],
                                          "actor"     => userID,
                                          "object"    => %{"id"      => objectID,
                                                           "type"    => "Tombstone",
                                                           "deleted" => Calendar.strftime(DateTime.utc_now(),
                                                                                          "%Y-%m-%dT%H:%M:%SZ")}});

             send_resp(conn, 200, "OK");
    end
  end

  def follow(conn, %{"inbox"    => inbox,    "object" => objectID,
                     "username" => username, "action" => action}) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user   = Users.get_full_user!(id);
             userID = URL.to_string(user.actor.object.activity_pub_id);

             case action do
                 "do" -> ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    &(userID <> "/followed/" <> &1),
                                                    %{"@context" => "https://www.w3.org/ns/activitystreams",
                                                      "type"     => "Follow",
                                                      "actor"    =>   userID,
                                                      "object"   => objectID,
                                                      "username" => username});
               "undo" -> actURL = URL.to_string(List.first(ActivityPubActivities.get_activity_by_type_object_and_actors("Follow",
                                                                                                                        objectID,
                                                                                                                        [userID]))
                                                    .object
                                                    .activity_pub_id);

                         ActivityPubUtils.send_json(user,
                                                    [inbox],
                                                    fn(_num) -> actURL <> "/undo"; end,
                                                    %{"@context" => "https://www.w3.org/ns/activitystreams",
                                                      "type"     => "Undo",
                                                      "actor"    => userID,
                                                      "object"   => actURL,
                                                      "username" => username});
             end

             send_resp(conn, 200, "OK");
    end
  end

  def post(conn, %{"content_warning"  => contentWarning,
                   "modal_editor"     => postText,
                   "privacy_level"    => privacyLevel,
                   "in_reply_to_post" => inReplyToPost} = params) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> pubTime   = Calendar.strftime(DateTime.utc_now(),
                                           "%Y-%m-%dT%H:%M:%SZ");
             user      = Repo.preload(Users.get_user!(id),
                                      [actor: [[:inbox, :shared_inbox_endpoint],
                                               [followers: [actor_items: [:inbox,
                                                                          :shared_inbox_endpoint]]]]]);
             userID    = DatabaseUtils.to_ap_id(user.actor);
             images    = params |> Enum.filter(&match?({"image"   <> _id, %Plug.Upload{}}, &1));
             captions  = params |> Enum.filter(&match?({"caption" <> _id,       _caption}, &1)) |> Map.new();
             {content,
              tags,
              tos}     = Enum.reduce((Regex.scan(~r/(?:^|\s)@([^\s]+)@((?:[A-Za-z0-9.-]+)(?::[0-9]+)?)(?<!\.)(?<!-)(?:[^A-Za-z0-9]|$)/,
                                                 postText) |> Enum.uniq_by(&(Enum.drop(&1, 1))))                            ++
                                     (~r/(?:^|\s)#([a-zA-Z_]+)/ |> Regex.scan(postText) |> Enum.uniq_by(&(Enum.at(&1, 1)))),
                                     {Earmark.as_html!(postText), [], []},
                                     fn(data, result) ->
                                       case data do
                                         [_foundStr, username,  domain] ->
                                           case ActivityPubActors.get_actor_by_pref_name(username,
                                                                                         domain)   do
                                             nil   -> result;
                                             actor -> {Regex.replace(~r/#{Regex.escape("@#{username}@#{domain}")}([^A-Za-z0-9]|$)/,
                                                                     elem(result, 0),
                                                                     "<span class=\"h-card\">"                                       <>
                                                                       "<a href=\"#{URL.to_string(actor.object.activity_pub_id)}\" " <>
                                                                          "class=\"u-url mention\">"                                 <>
                                                                         "@<span>#{username}</span>@<span>#{domain}</span>"          <>
                                                                       "</a>"                                                        <>
                                                                     "</span>\\g{1}"),
                                                       elem(result, 1) ++ [%{"type" => "Mention",
                                                                             "href" => actor,
                                                                             "name" => "@#{username}@#{domain}"}],
                                                       elem(result, 2) ++ [Repo.preload(actor, [:inbox, :shared_inbox_endpoint])]};
                                           end;
                                         [_foundStr, hashtag] ->
                                           {Regex.replace(~r/#{Regex.escape("##{hashtag}")}([^a-zA-Z_])/,
                                                          elem(result, 0),
                                                          "<a class=\"mention hashtag\" "                                  <>
                                                             "href=\"#{Routes.url(SwanyeWeb.Endpoint)}/tags/#{hashtag}\" " <>
                                                             "rel=\"tag\">"                                                <>
                                                            "#<span>#{hashtag}</span>"                                     <>
                                                          "</a>\\g{1}"),
                                            elem(result, 1) ++ [%{"type" => "Hashtag",
                                                                  "href" => "#{Routes.url(SwanyeWeb.Endpoint)}/tags/#{hashtag}",
                                                                  "name" => "##{String.downcase(hashtag)}"}],
                                            elem(result, 2)};
                                       end
                                     end);
             replyAddr = if(inReplyToPost !== ""               &&
                            privacyLevel  === "followers_post") do
                           with %{addressing_to_actors:      actors,
                                  addressing_to_collections: collections} <-
                                    Repo.preload(ActivityPubFetch.fetch_object(inReplyToPost),
                                                 [addressing_to_actors:      [addressee: [:inbox,
                                                                                          :shared_inbox_endpoint]],
                                                  addressing_to_collections: [addressee: [actor_items: [:inbox,
                                                                                                        :shared_inbox_endpoint]]]]),
                                %{to: allTos, cc: allCCs}                 <- Enum.group_by(actors ++ collections,
                                                                                           &(String.to_atom(&1.addressing_type)),
                                                                                           &(DatabaseUtils.to_ap_id(&1.addressee))) do
                             %{inboxes: Enum.map(Enum.map(     actors, &(&1.addressee))             ++ [user.actor] ++
                                                 Enum.map(collections, &(&1.addressee.actor_items)) ++ tos,
                                                 &(if(&1.shared_inbox_endpoint) do
                                                     URL.to_string(&1.shared_inbox_endpoint)
                                                   else
                                                     DatabaseUtils.to_ap_id(&1.inbox)
                                                   end)) |> Enum.uniq_by(&Function.identity/1),
                               to:      allTos ++ DatabaseUtils.to_ap_ids(tos) ++ [userID],
                               cc:      allCCs}
                           end
                         else
                           with public <- ["https://www.w3.org/ns/activitystreams#Public"] do
                             %{inboxes: Enum.map(user.actor.followers.actor_items ++ [user.actor] ++ tos,
                                                 &(if(&1.shared_inbox_endpoint) do
                                                     URL.to_string(&1.shared_inbox_endpoint)
                                                   else
                                                     DatabaseUtils.to_ap_id(&1.inbox)
                                                   end)) |> Enum.uniq_by(&Function.identity/1),
                               to:      (if privacyLevel ===   "public_post", do: public, else: []) ++ DatabaseUtils.to_ap_ids(tos)
                                                                                                    ++ [userID]
                                                                                                    ++ [DatabaseUtils.to_ap_id(user.actor
                                                                                                                                   .followers)],
                               cc:      (if privacyLevel === "unlisted_post", do: public, else: [])};
                           end
                         end;

             # IO.puts(~r/(^|\s)@((?:(?!<\/|\s).)+)@((?:(?!<\/|\s).)+)(<\/|\s|$)/
             #         |> Regex.scan(String.replace(postText, "\r", ""))
             #         |> Enum.reduce({postText, []},
             #                        fn([_foundStr, _group1, 
             #                            username,  domain,  _group4], result) ->
             #                          case ActivityPubActors.get_actor_by_pref_name(username,
             #                                                                        domain)   do
             #                            nil   -> result;
             #                            actor -> {String.replace(elem(result, 0),
             #                                                     "@#{username}@#{domain}",
             #                                                     "<span class=\"h-card\">" <>
             #                                                       "<a href=\"#{URL.to_string(actor.object.activity_pub_id)}\" " <>
             #                                                          "class=\"u-url mention\">" <>
             #                                                         "@<span>#{username}</span>@<span>#{domain}</span>" <>
             #                                                       "</a>" <>
             #                                                     "</span>"),
             #                                      elem(result, 1) ++ [%{type: "Mention",
             #                                                            href: actor.object.activity_pub_id,
             #                                                            name: "@#{username}@#{domain}"}]};
             #                          end
             #                        end)
             #         |> elem(0)
             #         |> Earmark.as_html!());

             if(length(images) > 0) do
               if(!Enum.any?(ExAws.request!(ExAws.S3.list_buckets()).body.buckets,
                             &(&1.name === "Swanye"))) do
                 ExAws.request!(ExAws.S3.put_bucket("Swanye",
                                                    ExAws.Config.new(:s3).region));
               end
             end

             obj = %{"@context"  => ["https://www.w3.org/ns/activitystreams",
                                     %{"sensitive" => "as:sensitive"}],
                     "type"      => "Create",
                     "to"        => replyAddr.to,
                     "cc"        => replyAddr.cc,
                     "published" => pubTime,
                     "actor"     => userID,
                     "object"    => %{"attributedTo" => [userID],
                                      "name"         => (if !params["title"]       or
                                                            params["title"] === "", do: nil, else: params["title"]),
                                      "summary"      => (if(Enum.all?(Enum.filter(params,
                                                                                  &(String.starts_with?(elem(&1, 0),
                                                                                                        "content_warning_checkbox_"))),
                                                                      &(elem(&1, 1)))) do
                                                           contentWarning;
                                                         else
                                                           nil;
                                                         end),
                                      "content"      => content,
                                      "tag"          => tags,
                                      "published"    => pubTime,
                                      "sensitive"    => false,
                                      "inReplyTo"    => inReplyToPost,
                                      "attachment"   => Enum.map(images,
                                                                 fn({"image" <> imageID, image}) ->
                                                                   binary  = File.read!(image.path);
                                                                   {"image/" <> ext,
                                                                    width,
                                                                    height,
                                                                    _type} = ExImageInfo.info(binary);
                                                                   usernm  = Base.url_encode64(user.actor.preferred_username);
                                                                   time    = Base.url_encode64(Calendar.strftime(DateTime.utc_now(),
                                                                                               "%a, %d %b %Y %H:%M:%S GMT"));
                                                                   mixed   = Enum.zip_reduce([String.to_charlist(usernm),
                                                                                              String.to_charlist(time)],
                                                                                             "",
                                                                                             fn(next, orig) ->
                                                                                               orig <> (if(:rand.uniform(2) === 2) do
                                                                                                          &String.reverse/1;
                                                                                                        else
                                                                                                          &Function.identity/1;
                                                                                                        end).(List.to_string(next));
                                                                                             end);

                                                                   %{"height"    => height,
                                                                     "mediaType" => image.content_type,
                                                                     "name"      => case Map.get(captions, "caption#{imageID}") do
                                                                                      nil     -> nil;
                                                                                      ""      -> nil;
                                                                                      caption -> caption;
                                                                                    end,
                                                                     "type"      => "Image",
                                                                     "url"       => ("Swanye" |> ExAws.S3.put_object("posts/#{mixed}.#{ext}",
                                                                                                                     binary,
                                                                                                                     [acl: :public_read])
                                                                                              |> ExAws.request!()).request_url,
                                                                     "width"     => width};
                                                                 end),
                                      "to"           => replyAddr.to,
                                      "cc"           => replyAddr.cc,
                                      "type"         => "Note"}};

             case ActivityPubUtils.send_json(user,
                                             replyAddr.inboxes,
                                             &(&1 <> "/activity"),
                                             obj,
                                             &(userID <> "/statuses/" <> &1)) do
               {:ok,    _activity} -> send_resp(conn,
                                                200,
                                                Jason.encode!(obj));
               {:error, _changset} -> send_resp(conn,
                                                400,
                                                "Something went wrong with posting");
             end
    end
  end
end
