defmodule Swanye.SwanyeActivityEffects do
  alias Swanye.Repo;
  alias Kabare.ActivityPubActivities;

  def create_effect(activity) do
    for(timeline <- activity.timelines) do
      if(timeline.timeline_type === "HOME"                 and
         timeline.user_id not in Enum.map(activity.actors,
                                          &(&1.actor_id)))  do
        SwanyeWeb.Endpoint
                 .broadcast("live:notifications_and_counts_#{timeline.user_id}",
                            "timeline",
                            %{"id"   => timeline.timeline_ordering_id,
                              "add"  => true,
                              "type" => "Create"});
      end
    end
  end

  def like_effect(activity) do
    for(notif <- activity.notifications) do
      SwanyeWeb.Endpoint.broadcast("live:notifications_and_counts_#{notif.user_id}",
                                   "notification",
                                   %{"notification" => notif,
                                     "add"          => true});
    end
  end

  def announce_effect(activity) do
    for(notif <- activity.notifications) do
      SwanyeWeb.Endpoint.broadcast("live:notifications_and_counts_#{notif.user_id}",
                                   "notification",
                                   %{"notification" => notif,
                                     "add"          => true});
    end
  end

  def follow_effect(activity) do
    for(notif <- activity.notifications) do
      SwanyeWeb.Endpoint.broadcast("live:notifications_and_counts_#{notif.user_id}",
                                   "notification",
                                   %{"notification" => notif,
                                     "add"          => true});
    end
  end

  defp p_undo(actedUponObj) do
    # this no longer works because we're removing the notification
    # from the database before we can do this part
    # We'll need to rethink this (maybe save the activity ID, too; I dunno)
    for(notif <- (actedUponObj.db_id |> ActivityPubActivities.get_activity!()
                                     |> Repo.preload([[notifications: :user]])).notifications) do
      SwanyeWeb.Endpoint.broadcast("live:notifications_and_counts_#{notif.user.user_id}",
                                   "notification",
                                   %{"notification" => notif,
                                     "add"          => false});
    end
  end
  def undo_effect(activity) do
    case activity.acted_upon_object.object_type do
      auo when auo in ["Announce",
                           "Like",
                         "Follow"] -> p_undo(activity.acted_upon_object);
    end
  end

  def activity_effect(activity) do
    case activity.object.object_type do
      "Create"   ->   create_effect(activity);
      "Like"     ->     like_effect(activity);
      "Announce" -> announce_effect(activity);
      "Follow"   ->   follow_effect(activity);
      "Undo"     ->     undo_effect(Repo.preload(activity, :acted_upon_object));
    end
  end
end
