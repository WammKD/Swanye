defmodule SwanyeWeb.AuthController do
  use SwanyeWeb, :controller;

  import Kabare.Utils.KabareUtils;

  alias Kabare.Users;
  alias Swanye.SwanyeUsers;
  alias Swanye.SwanyeUsers.User;

  plug(:put_layout, "auth.html");



  def new(conn, _params) do
    changeset = SwanyeUsers.change_user();
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => userParams}) do
    case SwanyeUsers.create_user(userParams) do
      {:ok,                             user} -> conn |> put_flash(:info,
                                                                   "User created successfully.")
                                                      |> render("show.html",
                                                                user: user);
      {:error, %Ecto.Changeset{} = changeset} -> render(conn,
                                                        "new.html",
                                                        changeset: changeset);
    end
  end
  def create(conn, %{"actor" => userParams}) do
    create(conn, %{"user" => userParams});
  end



  def confirmation(conn, %{"token" => confToken}) do
    if_let(user,
           SwanyeUsers.get_user_by_confirmation_token(confToken),
           do:   case SwanyeUsers.update_confirmation_token(user) do
                   {:ok,                             user} -> render(conn,
                                                                     "confirmation_success.html",
                                                                     user: user)
                   {:error, %Ecto.Changeset{} = changeset} -> render(conn,
                                                                     "new.html",
                                                                     changeset: changeset)
                 end,
           else: render(conn, "confirmation_error.html"));
  end



  def sign_in(conn, _params) do
    if(Plug.Conn.get_session(conn, "current_user")) do
      Phoenix.Controller.redirect(conn, to: Routes.page_path(conn, :index));
    else
      render(conn, "sign_in.html", changeset: SwanyeUsers.change_user());
    end
  end

  defp throw_error(connection) do
    connection |> put_flash(:info, "Incorrect username or password.")
               |> render("sign_in.html",
                         changeset: SwanyeUsers.change_user());
  end
  def authorize(conn, %{"user" => %{"password"           => pw,
				    "preferred_username" => username}}) do
    case Users.get_user_by_username(username) do
      nil                     -> throw_error(conn);
      %User{user_id:                   id,
            confirmation_token:     token,
            encrypted_password: encrypted} ->
          if(token === "confirmed") do
            if(User.valid_password?(pw, encrypted)) do
              # conn |> Plug.Conn.put_session(:current_user_id, id)
              #      |> (&(Phoenix.Controller.redirect(&1, to: Routes.page_path(&1, :index)))).();
              temp = Plug.Conn.put_session(conn, :current_user, id);

              Phoenix.Controller.delete_csrf_token();
              Phoenix.Controller.redirect(temp, to: Routes.page_path(temp, :index));
            else
              throw_error(conn);
            end
          else
            throw_error(conn);
          end
    end
  end

  def sign_out(conn, _params) do
    configure_session(conn, drop: true) |> Phoenix.Controller.redirect(to: Routes.auth_path(conn, :sign_in));
  end
end
