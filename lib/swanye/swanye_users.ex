defmodule Swanye.SwanyeUsers do
  @moduledoc """
  The ActivityPub context.
  """

  import Ecto.Query, warn: false;
  alias  Swanye.Repo;
  alias  Kabare.Utils.ConfigUtils;
  alias  Kabare.Users;
  alias  Swanye.SwanyeUsers.User;

  @doc false
  def get_user_by_confirmation_token(token) do
    Repo.one(from(u in User, where:  u.confirmation_token == ^token));
  end

  @doc false
  def create_user(attrs \\ %{}) do
    case %User{} |> User.changeset_new(attrs) |> Repo.insert() do
      {:ok, user} = uTuple -> Swanye.Email.create(user.e_mail,
                                                  "no-reply@swanye.social",
                                                  "NO REPLY: Account Confirmation Needed",
                                                  "Please visit "                     <>
                                                    ConfigUtils.instance_origin()     <>
                                                    "/auth/confirmation?token="       <>
                                                    user.confirmation_token           <>
                                                    " to complete your registration.") |> Swanye.Mailer.deliver_now!();

                              Users.create_user_id(user);

                              uTuple;
      {:error, _} = eTuple -> eTuple;
    end
  end

  def update_confirmation_token(%User{} = user), do: Repo.update(User.changeset_token(user));

  @doc false
  def update_user_summary_and_timezone(%User{} = user,
                                       summary,
                                       timezone)       when is_binary(summary)  and
                                                            is_binary(timezone) do
    user |> User.changeset_update_summary_and_timezone(summary,
                                                       timezone) |> Repo.update();
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %LoginUser{}}

  """
  def change_user() do
    User.changeset_blank();
  end
end
