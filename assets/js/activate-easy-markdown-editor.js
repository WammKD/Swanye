function keyDown(mde) {
  return function(_instance, event) {
           if(event.key === "Enter"      ||
              event.key === "ArrowUp"    ||
              event.key === "ArrowDown"  ||
              event.key === "ArrowLeft"  ||
              event.key === "ArrowRight") {
             const list = mde.gui
                             .toolbar
                             .nextElementSibling
                             .querySelector(".CodeMirror-scroll")
                             .querySelector("div#callout-users-list");

             if(list && list !== null && list !== undefined) {
               if(event.key === "ArrowLeft" || event.key === "ArrowRight") {
                 list.remove();
               } else {
                 const selected = list.querySelector(".selected");

                 if(selected && selected !== null && selected !== undefined) {
                   event.preventDefault();

                   if(event.key === "ArrowUp" || event.key === "ArrowDown") {
                     const sibling = event.key === "ArrowDown" ?
                                     selected.nextSibling      :
                                     selected.previousSibling;

                     if(sibling               &&
                        sibling !== null      &&
                        sibling !== undefined) {
                       selected.classList.remove("selected");
                       sibling.classList.add("selected");
                       sibling.scrollIntoView({ behavior: "smooth",
                                                block:    "nearest",
                                                inline:   "nearest" });
                     }
                   } else if(event.key === "Enter") {
                     const cursorInfo  = mde.codemirror.getCursor(),
                           workingLine = mde.value()
                                            .split("\n")[cursorInfo.line]
                                            .split(" ");
                     let   lineIndex   = 0;

                     for(let i = 0; i < workingLine.length; i++) {
                       lineIndex += workingLine[i].length + (i > 0 ? 1 : 0);

                       if(lineIndex >= cursorInfo.ch     &&
                          workingLine[i].startsWith("@")) {
                         mde.codemirror
                            .replaceSelection(selected.querySelector(".username")
                                                      .innerHTML
                                                      .substring(workingLine[i].length) + " ");
                         list.remove();

                         break;
                       }
                     }
                   }
                 } else {
                   list.remove();
                 }
               }
             }
           }
         };
}
function change(mde) {
  return function(_instance, changeObj) {
           const workingLine = mde.value()
                                  .split("\n")[changeObj.from
                                                        .line]
                                  .split(" "),
                 xhttp       = new XMLHttpRequest();
           let   lineIndex   = 0,
                 removeList  = true;

           for(let i = 0; i < workingLine.length; i++) {
             lineIndex += workingLine[i].length + (i > 0 ? 1 : 0);

             if(lineIndex >= changeObj.from.ch                &&
                workingLine[i].startsWith("@")                &&
                !(workingLine[i + 1]           !==      null &&
                  workingLine[i + 1]           !== undefined &&
                  workingLine[i + 1].valueOf() ===        "")) {
               removeList = false;

               xhttp.onreadystatechange = function() {
                                            if(this.readyState == 4   &&
                                               this.status     == 200) {
                                              const area    = mde.gui
                                                                 .toolbar
                                                                 .nextElementSibling
                                                                 .querySelector(".CodeMirror-scroll"),
                                                    cursor  = area.querySelector(".CodeMirror-cursor");
                                              let   list    = area.querySelector("div#callout-users-list");

                                              if(list               &&
                                                 list !== null      &&
                                                 list !== undefined) {
                                                while(list.firstChild) {
                                                  list.removeChild(list.lastChild);
                                                }
                                              } else {
                                                list = document.createElement("div");

                                                list.setAttribute("id", "callout-users-list");
                                                list.style.top  = "calc(" + cursor.style.top    +
                                                                  " + "   + cursor.style.height +
                                                                  " + "   + "7.5px"             + ")";
                                                list.style.left = "calc(" + cursor.style.left   +
                                                                  " - "   + "0.4em"             + ")";

                                                area.appendChild(list);
                                              }

                                              JSON.parse(this.responseText)
                                                  .forEach(userInfo => {
                                                             const info = document.createElement("div"),
                                                                   user = document.createElement("div"),
                                                                   name = document.createElement("div"),
                                                                   icon = document.createElement("img");

                                                             user.setAttribute("class", "username ellipsize");
                                                             user.innerHTML = userInfo.handle;

                                                             name.setAttribute("class", "name     ellipsize");
                                                             name.innerHTML = userInfo.name;

                                                             icon.setAttribute("class", "icon");
                                                             icon.setAttribute("src",   userInfo.icon);

                                                             info.onclick = function() {
                                                                              mde.gui
                                                                                 .toolbar
                                                                                 .nextElementSibling
                                                                                 .querySelector("div#callout-users-list")
                                                                                 .remove();

                                                                              mde.codemirror
                                                                                 .replaceSelection(userInfo.handle
                                                                                                           .substring(workingLine[i].length) + " ");
                                                                            };
                                                             info.setAttribute("class", "user-info");
                                                             info.appendChild(user);
                                                             info.appendChild(name);
                                                             info.appendChild(icon);

                                                             list.appendChild(info);
                                                           });

                                              if(list.firstChild               &&
                                                 list.firstChild !== null      &&
                                                 list.firstChild !== undefined) {
                                                list.firstChild.classList.add("selected");
                                              }
                                            } else if(this.readyState == 4) {
                                              console.log(this.responseText);
                                            }
                                          };
               xhttp.open("POST", "/frontend/search_by_partial_name", true);
               xhttp.setRequestHeader("Content-Type", "application/json");
               xhttp.send(JSON.stringify({"_csrf_token": document.getElementById("meta-csrf")
                                                                 .getAttribute("content"),
                                          "search"     : workingLine[i].substring(1)          }));

               break;
             }
           }

           if(removeList) {
             const lst = mde.gui
                            .toolbar
                            .nextElementSibling
                            .querySelector("div#callout-users-list");

             if(lst && lst !== null && lst !== undefined) {
               lst.remove();
             }
           }
         };
}

function activateMDE(textarea) {
  const easy = new EasyMDE({ element:       textarea,
                             toolbar:       ["bold",           "italic",          "strikethrough", "|",
                                             "heading",        "horizontal-rule",                  "|",
                                             "quote",          "code",            "clean-block",   "|",
                                             "unordered-list", "ordered-list",                     "|",
                                             "link",           "image",                            "|",
                                             "preview",        "side-by-side",    "fullscreen",    "|",
                                             "guide"],
                             minHeight:     "200px",
                             /* previewRender: (text) => processor.processSync(text).value, */
                             blockStyles:   { bold: "**", italic: "_" },
                             insertTexts:   { image: ["![don't forget alt. text!](https://", ")"] } }),
        foot = textarea.parentElement.nextElementSibling;

  easy.codemirror.on("keydown", keyDown(easy));
  easy.codemirror.on("change",   change(easy));

  textarea.parentElement
          .querySelectorAll("button[type=button].easyFormSubmit")
          .forEach(function(button) {
                     button.onclick = function() {
                                        const xhttp = new XMLHttpRequest(),
                                              form  = textarea.parentElement;
                                        function buttonMod(disable) {
                                          form.querySelectorAll("a.close")
                                              .forEach((elem) => elem.setAttribute("style",
                                                                                   disable ? "display: none" : ""));
                                          form.querySelectorAll("a.close                ~ button[type=button].easyFormSubmit, " +
                                                                "div.confirm-or-decline > button[type=button]")
                                              .forEach((elem) => disable ? elem.setAttribute("disabled", "")
                                                                         : elem.removeAttribute("disabled"));
                                        }

                                        textarea.innerHTML = easy.value();

                                        buttonMod(true);

                                        xhttp.onreadystatechange = function() {
                                                                     if(this.readyState == 4   &&
                                                                        this.status     == 200) {
                                                                       // console.log(JSON.parse(this.responseText));
                                                                       buttonMod(false);

                                                                       if(form.getAttribute("class")
                                                                              .startsWith("post-editor")) {
                                                                         form.classList
                                                                             .remove("show");
                                                                         foot.classList
                                                                             .remove("hide");
                                                                       } else {
                                                                         location.href = '#close-model';
                                                                       }

                                                                       easy.value("");
                                                                       form.querySelectorAll("input")
                                                                           .forEach((input) => {
                                                                                      if(input.name !== "_csrf_token" &&
                                                                                         input.type !== "radio") {
                                                                                        input.value = "";
                                                                                      }
                                                                                    });
                                                                     } else if(this.readyState == 4) {
                                                                       buttonMod(false);

                                                                       console.log(this.responseText);
                                                                     }
                                                                   };
                                        xhttp.open(form.getAttribute('method'),
                                                   form.getAttribute('action'),
                                                   true);
                                        xhttp.send(new FormData(form));
                                      };
                   });

  if(foot && foot !== null && foot !== undefined) {
    foot.querySelector("svg.icon-gridicons-comment")
        .onclick = function() {
                     this.parentElement
                         .previousElementSibling
                         .classList
                         .add("show");
                     this.parentElement
                         .classList
                         .add("hide");

                     if(easy.value() === "") {
                       easy.value("@" + this.parentElement
                                            .previousElementSibling
                                            .previousElementSibling
                                            .dataset
                                            .users + " ");
                     }
                   };
  }
}

export default activateMDE;
