(use-modules (gnu packages base)
             (gnu packages commencement)
             (gnu packages databases)
             (gnu packages elixir)
             (gnu packages linux)
             (gnu packages node)
             (gnu packages tls))

(packages->manifest (list elixir node mariadb gnu-make gcc-toolchain inotify-tools openssl))
