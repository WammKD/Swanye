defmodule Swanye.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Swanye.Repo,
      # Start the Telemetry supervisor
      SwanyeWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Swanye.PubSub},
      # Start the Endpoint (http/https)
      SwanyeWeb.Endpoint
      # Start a worker by calling: Swanye.Worker.start_link(arg)
      # {Swanye.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Swanye.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SwanyeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
