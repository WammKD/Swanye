defmodule SwanyeWeb.Router do
  use SwanyeWeb,        :router;
  use KabareWeb.Router;

  pipeline(:browser) do
    plug(:accepts, ["html"]);
    plug(:fetch_session);
    plug(:fetch_live_flash);
    plug(:fetch_flash);
    plug(:protect_from_forgery);
    plug(:put_secure_browser_headers);
  end

  pipeline(:auth_layout) do
    plug(:put_layout, {SwanyeWeb.LayoutView, :auth});
  end

  pipeline(:users_layout) do
    plug(:accepts, ["html", "json", "jsonld", "activity+json"]);
    plug(:fetch_session);
    plug(:fetch_flash);
    plug(:protect_from_forgery);
    plug(:put_secure_browser_headers);
    plug(:put_layout, {SwanyeWeb.LayoutView, :users});
  end
  pipeline(:users_layout_without_protect) do
    plug(:accepts, ["html", "json", "jsonld", "activity+json"]);
    plug(:fetch_session);
    plug(:fetch_flash);
    plug(:put_secure_browser_headers);
    plug(:put_layout, {SwanyeWeb.LayoutView, :users});
  end

  pipeline(:search_layout) do
    plug(:put_layout, {SwanyeWeb.LayoutView, :search});
  end

  pipeline(:account_layout) do
    plug(:put_layout, {SwanyeWeb.LayoutView, :account});
  end

  pipeline(:dashboard_layout) do
    plug(:put_layout, {SwanyeWeb.LayoutView, :dashboard});
  end

  pipeline(:frontend_handling) do
    plug(:accepts, ["json", "jsonld", "activity+json"]);
    plug(:fetch_session);
    plug(:protect_from_forgery);
    plug(:put_secure_browser_headers);
  end

  pipeline(:api) do
    plug(:accepts, ["json", "jsonld", "activity+json"]);
  end

  scope "/", SwanyeWeb do
    pipe_through(:browser);

    get("/", PageController, :index);
  end

  scope "/", SwanyeWeb do
    pipe_through([:browser, :auth_layout]);

     get("/auth/sign_up",      AuthController, :new);
    post("/auth/sign_up",      AuthController, :create);

     get("/auth/confirmation", AuthController, :confirmation);

     get("/auth/sign_in",      AuthController, :sign_in);
    post("/auth/sign_in",      AuthController, :authorize);

     get("/auth/sign_out",     AuthController, :sign_out);
  end

  scope "/", SwanyeWeb do
    pipe_through([:users_layout]);

    get("/users/:actor",                  UsersController, :profile);
    get("/@:actor",                       UsersController, :profile);

    get("/users/:actor/followers",        UsersController, :followers);
    get("/@:actor/followers",             UsersController, :followers);

    get("/users/:actor/following",        UsersController, :following);
    get("/@:actor/following",             UsersController, :following);

    get("/users/:actor/statuses/:postID", UsersController, :post);
    get("/@:actor/:postID",               UsersController, :post);
  end
  scope "/", SwanyeWeb do
    pipe_through([:users_layout_without_protect]);

    post("/users/:username/inbox",             UsersController, :inbox);
    post("/inbox",                             UsersController, :shared_inbox);
  end

  scope "/", SwanyeWeb do
    pipe_through([:browser, :search_layout]);

    get("/search", SearchController, :search);
  end

  scope "/", SwanyeWeb do
    pipe_through([:browser, :account_layout]);

     get("/settings/account",       AccountController, :settings);
    post("/settings/account",       AccountController, :save);

     get("/settings/notifications", AccountController, :notifications);
  end

  scope "/", SwanyeWeb do
    pipe_through([:frontend_handling]);

    post("/frontend/search_by_partial_name", FrontendHandlingController, :search_by_partial_name);
    post("/frontend/like",                   FrontendHandlingController, :like);
    post("/frontend/reblog",                 FrontendHandlingController, :reblog);
    post("/frontend/delete",                 FrontendHandlingController, :delete);
    post("/frontend/follow",                 FrontendHandlingController, :follow);
    post("/frontend/post",                   FrontendHandlingController, :post);
  end

  scope "/", SwanyeWeb do
    pipe_through([:browser, :dashboard_layout]);

    
  end

  # Other scopes may use custom stacks.
  # scope "/api", SwanyeWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SwanyeWeb.Telemetry
    end
  end
end
