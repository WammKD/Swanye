defmodule Swanye.Repo.Migrations.AlterUsers do
  use Ecto.Migration;

  def change do
    alter table(:users) do
      add(:e_mail,              :string, null: false, size:    320);
      add(:timezone,            :string, null: false, default: "Etc/UTC");
      add(:encrypted_password,  :text,   null: false);
      add(:confirmation_token,  :string, null: false);
      add(:summary_as_markdown, :text);
    end
  end
end
