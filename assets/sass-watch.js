// https://mfeckie.dev/sass-in-phoenix/

const sass = require("sass");
const sane = require("sane");
const fs   = require("fs");

process.stdin.on("end", () => process.exit());

process.stdin.resume();

function styleChanged(filepath) {
  const before = new Date();

  fs.readdir("./css",
             function(err, files) {
               //handling error
               if(err) {
                 const { css } = renderSCSS(filepath);

                 console.log(`CSS rebuilt in ${new Date() - before}ms`);

                 fs.writeFileSync("../priv/static/assets/"                   +
                                  (filepath ? filepath.slice(0, -5) : "app") + ".css", css);

                 return console.log("Unable to scan directory: " + err);
               } else {
                 //listing all files using forEach
                 files.filter(function(file) {
                                return file.toLowerCase().endsWith(".scss");
                              })
                      .forEach(function(file) {
                                 const { css } = renderSCSS(file);

                                 console.log(`CSS rebuilt in ${new Date() - before}ms`);

                                 fs.writeFileSync("../priv/static/assets/"           +
                                                  (file ? file.slice(0, -5) : "app") + ".css", css);
                               });
               }
             });
}
function renderSCSS(file_path) {
  return sass.renderSync({ file:           "css/" + (file_path ? file_path
                                                               : "app.scss"),
                           sourceMapEmbed: true                              });
}

const styleWatcher = sane("css", { glob: ["**/*.scss"] });

styleWatcher.on("ready", styleChanged);

styleWatcher.on("add",    styleChanged);
styleWatcher.on("delete", styleChanged);
styleWatcher.on("change", styleChanged);
