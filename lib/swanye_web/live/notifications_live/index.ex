defmodule SwanyeWeb.NotificationsLive.Index do
  use   SwanyeWeb, :live_view;
  alias Swanye.Repo;
  alias Kabare.Users.Notification;

  @impl true
  def mount(_params, session, socket) do
    user = session["user"];

    if(!is_nil(user) and connected?(socket)) do
      SwanyeWeb.Endpoint.subscribe("live:notifications_and_counts_#{user.user_id}");
    end

    {:ok, assign(socket, user:          user,
                         notifications: Enum.reverse(user.notifications))};
  end

  @impl true
  def handle_info(%Phoenix.Socket.Broadcast{event:   "notification",
                                            payload: %{"notification" => notif,
                                                       "add"          => add?}}, socket) do
    noti = Repo.preload(notif,
                        [activity: [:object, :actors, :acted_upon_object]]);

    {:noreply, assign(socket,
                      notifications: if(add?) do
                                       [noti] ++ socket.assigns.notifications;
                                     else
                                       Enum.reject(socket.assigns.notifications,
                                                   &(  &1.notification_ordering_id ===
                                                     noti.notification_ordering_id));
                                     end)};
  end

  def first_actor(%Notification{} = notification) do
    Enum.at(notification.activity.actors, 0);
  end
end
