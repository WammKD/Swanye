const sass = require("sass"),
      fs   = require("fs");


fs.mkdir("../priv/static/assets",
         { recursive: true },
         (err) => { if(err) { throw err }; });

fs.readdir("css",
           function(err, files) {
             if(!err) {
               files.forEach(function(file) {
                               if(file.slice(-5) === ".scss") {
                                 const { css } = sass.renderSync({ file:           "css/" + file,
                                                                   sourceMapEmbed: true          });

                                 fs.writeFileSync("../priv/static/assets/" +
                                                  file.slice(0, -5)        + ".css", css);
                               }
                             });
             }
           });
