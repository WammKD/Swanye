defmodule Swanye.SwanyeUsers.User do
  use    Ecto.Schema;
  import Ecto.Changeset;

  alias  Swanye.Repo;
  alias  Kabare.Utils.ConfigUtils;
  alias  Kabare.Utils.DatabaseUtils;
  alias  Kabare.URLs.URL;
  alias  Swanye.SwanyeUsers.User;
  alias  Kabare.Users.Notification;
  alias  Kabare.ActivityPubActors.Actor;
  alias  Kabare.ActivityPubActivities.Activity;
  alias  Kabare.ActivityPubCollections;

  @primary_key {:user_id, :id, autogenerate: true}
  schema "users" do
    field(:confirmation_token,    :string);
    field(:e_mail,                :string);
    field(:timezone,              :string);
    field(:encrypted_password,    :string);
    field(:preferred_username,    :string, virtual: true);
    field(:password,              :string, virtual: true);
    field(:password_confirmation, :string, virtual: true);
    field(:private_key,           :string);
    field(:public_key,            :string);
    field(:summary_as_markdown,   :string);

    belongs_to(:actor,                Actor,               primary_key: true,
               references: :actor_id, define_field: false, foreign_key: :user_id);

    many_to_many(:timeline_posts,                   Activity,
                 join_through: "timelines",         join_keys: [    user_id:     :user_id,
                                                                activity_id: :activity_id]);
        has_many(:notifications,                    Notification,
                 foreign_key:  :user_id,            references: :user_id);
        has_many(:activities_via_notification,      through: [:notifications, :activity]);

    many_to_many(:pending_followees,                Actor,
                 join_through: "following_pending", join_keys: [ user_id__follower:  :user_id,
                                                                actor_id__followee: :actor_id]);

    timestamps();
  end

  defimpl Jason.Encoder, for: [Swanye.SwanyeUsers.User] do
    def encode(%User{} = unpreloadedUser, opts) do
      Repo.preload(unpreloadedUser, [actor: :object]).actor.object.original_form;
    end
  end

  def non_null__fields, do: [:encrypted_password, :public_key,  :e_mail,
                             :confirmation_token, :private_key];

  @doc false
  def changeset_blank() do
    %User{} |> cast(%{}, DatabaseUtils.fields_without_time(User));
  end

  defp generate_collection(id) when is_binary(id) do
    URL.to_string((%{"id"         => id,
                     "type"       => "OrderedCollection",
                     "totalItems" => 0,
                     "items"      => []} |> ActivityPubCollections.fetch_collection(false)
                                         |> Repo.preload(:activity_pub_id)).activity_pub_id);
  end
  @doc false
  def changeset_new(%User{} = user, attrs) do
    {:ok, {priv, pub}} = RsaEx.generate_keypair
    id                 = ConfigUtils.instance_origin() <> "/users/" <> attrs["preferred_username"];
    json               = %{"@context"          => ["https://www.w3.org/ns/activitystreams",
                                                   "https://w3id.org/security/v1",
                                                   %{"manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
                                                     "publicKeyBase64"           => "toot:publicKeyBase64",
                                                     "featured"                  => %{"@type" => "@id",
                                                                                      "@id"   => "toot:featured"},
                                                     "featuredTags"              => %{"@id"   => "toot:featuredTags",
                                                                                      "@type" => "@id"}}],
                           "id"                => id,
                           "type"              => "Person",
                           "url"               => ConfigUtils.instance_origin() <> "/@" <> attrs["preferred_username"],
                           "inbox"             => generate_collection(id <> "/inbox"),
                           "endpoints"         => %{"sharedInbox" => generate_collection(ConfigUtils.instance_origin() <> "/inbox")},
                           "outbox"            => generate_collection(id <> "/outbox"),
                           "following"         => generate_collection(id <> "/following"),
                           "followers"         => generate_collection(id <> "/followers"),
                           "liked"             => generate_collection(id <> "/liked"),
                           "shares"            => generate_collection(id <> "/shares"),
                           "featured"          => generate_collection(id <> "/featured"),
                           "featuredTags"      => generate_collection(id <> "/tags"),
                           "publicKey"         => %{"id"           => id <> "#main-key",
                                                    "owner"        => id,
                                                    "publicKeyPem" => pub},
                           "preferredUsername" => attrs["preferred_username"]};

    user |> cast(Map.merge(%{"actor"              => json,
                             "confirmation_token" => Base.url_encode64(:crypto.strong_rand_bytes(128)),
                              "public_key"        => pub,
                             "private_key"        => priv},
                           attrs),
                 [:password, :password_confirmation, :preferred_username] ++
                 DatabaseUtils.fields_without_time(User))
         |> cast_assoc(:actor,
                       required: true,
                       with:     &(Actor.changeset(&1, &2, false)))
         |> validate_length(:password, min: 8)
         |> validate_confirmation(:password)
         |> validate_length(:preferred_username, min: 1, max: 64)
         |> validate_format(:preferred_username,
                            ~r/^\S+$/,
                            message: "Username cannot have whitespace.")
         |> is_valid_timezone?()
         |> encrypt_password()
         |> validate_required(User.non_null__fields);
  end
  def changeset_update_summary_and_timezone(%User{} = user,
                                            summary,
                                            timezone)       when is_binary(summary)  and
                                                                 is_binary(timezone) do
    user |> cast(%{summary_as_markdown: summary,
                   timezone:            timezone}, [:summary_as_markdown, :timezone]);
  end

  @doc false
  def changeset_token(%User{} = user) do
    user |> cast(%{confirmation_token: "confirmed"}, [:confirmation_token]);
  end

  def valid_password?(%User{}   = user),   do: valid_password?(user.password,
                                                               user.encrypted_password);
  def valid_password?(password,
                      encrypted_password), do: Argon2.verify_pass(password,
                                                                  encrypted_password);

  def is_valid_timezone?(changeset) do
    timezone = Ecto.Changeset.get_field(changeset, :timezone);

    if(!is_nil(timezone) and is_binary(timezone)) do
      if(Timex.is_valid_timezone?(timezone)) do
        changeset;
      else
        Ecto.Changeset.add_error(changeset,
                                 :timezone,
                                 "is not a valid timezone");
      end
    else
      changeset;
    end
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :password);

    if(password) do
      put_change(changeset, :encrypted_password, Argon2.hash_pwd_salt(password));
    else
      changeset
    end
  end
end
