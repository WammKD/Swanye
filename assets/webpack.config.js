const path = require('path');
const glob = require('glob');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, options) => {
  const devMode = options.mode !== 'production';

  return {
    optimization: {
      minimizer: [
        new TerserPlugin({ cache: true, parallel: true, sourceMap: devMode }),
        new OptimizeCSSAssetsPlugin({})
      ]
    },
    entry: {
      'service-worker': glob.sync('./vendor/**/*.js').concat(['./js/service-worker.js']),
      'font'          : glob.sync('./vendor/**/*.js').concat(['./js/font.js']),
      'app'           : glob.sync('./vendor/**/*.js').concat(['./js/app.js']),
      'auth'          : glob.sync('./vendor/**/*.js').concat(['./js/auth.js']),
      'main_layout'   : glob.sync('./vendor/**/*.js').concat(['./js/main_layout.js']),
      'side_buttons'  : glob.sync('./vendor/**/*.js').concat(['./js/side_buttons.js']),
      'navigation'    : glob.sync('./vendor/**/*.js').concat(['./js/navigation.js']),
      'profiles'      : glob.sync('./vendor/**/*.js').concat(['./js/profiles.js']),
      'users'         : glob.sync('./vendor/**/*.js').concat(['./js/users.js']),
      'posts'         : glob.sync('./vendor/**/*.js').concat(['./js/posts.js']),
      'warning_modal' : glob.sync('./vendor/**/*.js').concat(['./js/warning_modal.js']),
      'post_modal'    : glob.sync('./vendor/**/*.js').concat(['./js/post_modal.js']),
      'post_editor'   : glob.sync('./vendor/**/*.js').concat(['./js/post_editor.js']),
      'settings'      : glob.sync('./vendor/**/*.js').concat(['./js/settings.js']),
      'notifications' : glob.sync('./vendor/**/*.js').concat(['./js/notifications.js']),
      'search'        : glob.sync('./vendor/**/*.js').concat(['./js/search.js'])
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, '../priv/static/js'),
      publicPath: '/js/'
    },
    devtool: devMode ? 'eval-cheap-module-source-map' : undefined,
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.[s]?css$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                outputPath: '../'
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({ filename: '../css/[name].css' }),
      new CopyWebpackPlugin([{ from: 'static/', to: '../' }])
    ]
    .concat(devMode ? [new HardSourceWebpackPlugin()] : [])
  }
};
