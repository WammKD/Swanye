/* import { unified }   from 'unified';
 * import parse         from 'uniorg-parse';
 * import uniorg2rehype from 'uniorg-rehype';
 * import stringify     from 'rehype-stringify';
 * 
 * const processor = unified().use(parse).use(uniorg2rehype).use(stringify);
 * window.proc = processor; */

import activateMDE from "./activate-easy-markdown-editor";

document.querySelectorAll("form textarea#modal-editor, "       +
                          "form textarea#image_modal-editor, " +
                          "form textarea[name=modal_editor]")
        .forEach(activateMDE);
