defmodule Swanye.MixProject do
  use Mix.Project

  def project do
    [
      app: :swanye,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Swanye.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.6.11"},
      {:phoenix_ecto, "~> 4.5.1"},
      {:ecto_sql, "~> 3.11.1"},
      {:myxql, ">= 0.0.0"},
      {:phoenix_html, "~> 3.2"},
      {:phoenix_live_reload, "~> 1.3.3", only: :dev},
      {:phoenix_live_view, "~> 0.17.5"},
      {:phoenix_live_dashboard, "~> 0.6.5"},
      {:esbuild, "~> 0.4", runtime: Mix.env() == :dev},
      {:telemetry_metrics, "~> 0.6.1"},
      {:telemetry_poller, "~> 1.0.0"},
      {:gettext, "~> 0.24.0"},
      {:jason, "~> 1.3"},
      {:plug_cowboy, "~> 2.7.0"},
      {:argon2_elixir, "~> 3.1"},
      {:recase, "~> 0.7"},
      {:rsa_ex, "~> 0.4"},
      {:aes256, "~> 0.5.1"},
      {:bamboo, "~> 2.2.0"},
      {:httpoison, "~> 2.2.1"},
      {:earmark, "~> 1.4.26"},
      {:sweet_xml, "~> 0.7.3"},
      {:ex_aws, "~> 2.3.3"},
      {:ex_aws_s3, "~> 2.3.3"},
      {:ex_image_info, "~> 0.2.4"},
      {:timex, "~> 3.7.11"},
      {:kabare,
       git: "https://codeberg.org/WammKD/Kabar-",
       branch: "primary"},
      {:mock, "~> 0.3.7", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "cmd npm install --prefix assets"],
      "ecto.setup": ["ecto.create", "kabare_migrate", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "kabare_migrate", "ecto.migrate --quiet", "test"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"]
    ]
  end
end
