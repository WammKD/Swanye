defmodule Swanye.Email do
  import Bamboo.Email;

  def create(to, from, subject, body) do
    new_email(subject:   subject,
              from:      from,
              to:        case to do
                           addr -> [addr]
                           [_]  -> to
                         end,
              # html_body: html,
              text_body: body);
  end
end
