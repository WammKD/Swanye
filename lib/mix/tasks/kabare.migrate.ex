defmodule Mix.Tasks.KabareMigrate do
  use   Mix.Task;
  alias Swanye.Repo;

  def run(_args) do
    Mix.Task.run("app.start");

    Ecto.Migrator.run(Repo,
                      Application.app_dir(:kabare, "priv/test_repo/migrations"),
                      :up,
                      all: true);
  end
end
