# Code of Conduct

## This Repository

Swanye consists of libre and open-source code which allows for running the
Swanye web app.

Furtherance and development of this code is one of the major concerns of this
repository. As such, the nature of engagement and cooperation between those
contributing must be in the service of furthering and progressing the code, in
whatever form is most optimal.

As such, this Code of Conduct proceeds with the goal of creating a development
ecosystem that concerns itself with advancing the code. The rules and
regulations of this repository (and those who engage within it) are laid out
below, in aim of the previously stated goals.

## Harassment

This repository's code is most benefitted when those outside the original
creator contribute to its development, as one individual is severely less
capable of moving as quickly or catching bugs as efficiently as many people
working in concert.

As such, reducing any possible harassment to contributors is in the benefit of
the code, as a whole, as it creates an environment where others desire to get
involved and contribute.

Further, behavior and publicly espoused beliefs which target particular groups
and identities – especially those which advocate policing and eradicating
immutable identities/qualities – puts under threat those, and reduces the
number of people, who might – now and in the future – contribute to this
repository's development.

This repo. strictly is against such harassment and will take steps to ensure
that it is not feasible.

Harassment includes:

* Offensive comments related to
    * gender
    * gender identity and expression
    * sex characteristics
    * sexual orientation
    * disability
    * mental illness
    * neuro(a)typicality
    * physical appearance
    * body size
    * age
    * race
    * ethnicity
    * socioeconomic status
    * religion
* Unwelcome comments regarding a person’s lifestyle choices and practices,
including those related to
    * food
    * health
    * parenting
    * drugs
    * employment
* Deliberate misgendering or use of ‘dead’ or rejected names.
* Gratuitous or off-topic sexual images or behaviour in spaces where they’re not
appropriate.
* Physical contact and simulated physical contact (e.g. textual descriptions
like “*hug*” or “*backrub*”) after a request to stop.
* Threats of violence.
* Incitement of violence towards any individual, including encouraging a person
to commit suicide or to engage in self-harm.
* Deliberate intimidation.
* Stalking or following.
* Harassing photography or recording, including logging online activity for
harassment purposes.
* Sustained disruption of discussion.
* Unwelcome sexual attention.
* Continued one-on-one communication after requests to cease.
* Deliberate “outing” of any aspect of a person’s identity without their consent
except as necessary to protect vulnerable people from intentional abuse.
* Publication of non-harassing private communication.

## Scope

This Code of Conduct applies within all community spaces and, also, applies when
an individual is officially representing the community in public spaces.

Examples of representing our community include using an official e-mail address,
posting via an official social media account, or acting as an appointed
representative at an online or offline event.

## Reporting

If you are being harassed by a member of this repo., notice that someone else is
being harassed, or have any other concerns: please contact WammKD. If the person
who is harassing you is an official representative of this repo., they will
recuse themselves from handling your incident. We will respond as promptly as we
can.

In order to protect volunteers from abuse and burnout, we reserve the right to
reject any report we believe to have been made in bad faith. Reports intended to
silence legitimate criticism may be deleted without response.

We will respect confidentiality requests for the purpose of protecting victims
of abuse. At our discretion, we may publicly name a person about whom we’ve
received harassment complaints or privately warn third parties about them, if we
believe that doing so will increase the safety of members or the general public.
We will not name harassment victims without their affirmative consent.

## Enforcement Guidelines

Community leaders will follow these guidelines in determining the consequences
for any action they deem in violation of this Code of Conduct:

### 1. Correction

**Community Impact**: Use of inappropriate language or other behavior deemed
unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behavior was inappropriate. A public apology may be requested.

### 2. Warning

**Community Impact**: A violation through a single incident or series
of actions.

**Consequence**: A warning with consequences for continued behavior. No
interaction with the people involved, including unsolicited interaction with
those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels
like social media. Violating these terms may lead to a temporary or
permanent ban.

### 3. Temporary Ban

**Community Impact**: A serious violation of community standards, including
sustained inappropriate behavior.

**Consequence**: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

### 4. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community
standards, including sustained inappropriate behavior,  harassment of an
individual, or aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within
the community.

## Attribution

The [Scope](#scope) and [Enforcement Guidelines](#enforcement-guidelines) of
this Code of Conduct is adapted from the [Contributor Covenant][homepage],
version 2.1, available at
[https://www.contributor-covenant.org/version/2/1/code_of_conduct.html][v2.1].

Community Impact Guidelines were inspired by
[Mozilla's code of conduct enforcement ladder][Mozilla CoC].

For answers to common questions about the Contributor Covenant code of conduct,
see the FAQ at [https://www.contributor-covenant.org/faq][FAQ]. Translations are
available at [https://www.contributor-covenant.org/translations][translations].

[homepage]: https://www.contributor-covenant.org
[v2.1]: https://www.contributor-covenant.org/version/2/1/code_of_conduct.html
[Mozilla CoC]: https://github.com/mozilla/diversity
[FAQ]: https://www.contributor-covenant.org/faq
[translations]: https://www.contributor-covenant.org/translations
