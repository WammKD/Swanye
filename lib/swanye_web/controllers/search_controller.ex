defmodule SwanyeWeb.SearchController do
  use   SwanyeWeb, :controller;
  alias Swanye.Repo;
  alias Kabare.Users;
  alias Kabare.ActivityPubFetch;
  alias Kabare.ActivityPubActors;
  alias Kabare.ActivityPubObjects;
  alias Kabare.ActivityPubHashtags;

  def search(conn, %{"terms" => searchTerms}) do
    load_hash = fn(hashtags)        -> Repo.preload(hashtags, [:objects]); end;
    load_rest = fn(actorsOrObjects) ->
                  finalList = actorsOrObjects |> List.wrap()
                                              |> List.flatten()
                                              |> Enum.reject(&is_nil/1);

                  if(Enum.empty?(finalList)) do
                    [];
                  else
                    groups = Enum.group_by(finalList,
                                           &(case &1 do
                                               %ActivityPubActors.Actor{}   -> :actors;
                                               %ActivityPubObjects.Object{} -> :objects;
                                             end));
                    (Repo.preload(groups[:actors],
                                  [object: [images: [object: [url: [:origin,
                                                                    :path]]]]])                || []) ++
                    (Repo.preload(groups[:objects],
                                  [attributees: [object: [images: [object: [url: [:origin,
                                                                                  :path]]]]]]) || []);
                  end
                end;
    searches = cond do
                 # An ActivityPub ID
                 String.starts_with?(searchTerms, "http")    and
                 !Regex.match?(~r/[[:space:]]/, searchTerms)        ->
                   load_rest.(Repo.preload(ActivityPubFetch.fetch_actor(searchTerms),
                                           :inbox));
                 # Of the form =@user@domain= or =user@domain=
                 Regex.match?(~r/^@?[^\s@]+@[^\s@]+$/, searchTerms) ->
                   with withoutLeadingAt <- String.split(String.slice(searchTerms,
                                                                      (if String.starts_with?(searchTerms,
                                                                                              "@"), do: 1, else: 0)..-1),
                                                         "@") do
                     load_rest.(ActivityPubFetch.fetch_actor(List.first(withoutLeadingAt),
                                                             List.last(withoutLeadingAt)));
                   end;
                 # Of the form =@noSpacesWord=
                 Regex.match?(~r/^@[^\s@]+$/,          searchTerms) ->
                   with withoutAt        <- String.slice(searchTerms, 1..-1) do
                     load_rest.(ActivityPubActors.get_actor_by_pref_name_only(withoutAt) ++
                                ActivityPubActors.get_actor_by_authority_only(withoutAt));
                   end;
                 # Of the form =#hashtag=
                 Regex.match?(~r/^#[^\s#]+$/,          searchTerms) ->
                   load_hash.(ActivityPubHashtags.get_hashtags_by_partial_name(searchTerms));
                 # Everything else
                 true                                               ->
                   load_rest.(ActivityPubActors.get_actor_by_pref_name_only(searchTerms) ++
                              ActivityPubActors.get_actor_by_authority_only(searchTerms))    ++
                   load_hash.(ActivityPubHashtags.get_hashtags_by_partial_name(searchTerms));
               end;

    render(conn,
           "index.html",
           searches:     searches,
           loggedInUser: case Plug.Conn.get_session(conn, "current_user") do
                           nil -> nil;
                           id  -> Users.get_full_with_likes_and_followees!(id);
                         end,
           page:         :search)
  end

  def index(_conn, _params) do
    # searches = Whatever.list_searches()
    # render(conn, "index.html", searches: searches)
  end

  def new(_conn, _params) do
    # changeset = Whatever.change_search(%Search{})
    # render(conn, "new.html", changeset: changeset)
  end

  def create(_conn, %{"search" => _search_params}) do
    # case Whatever.create_search(search_params) do
    #   {:ok, search} ->
    #     conn
    #     |> put_flash(:info, "Search created successfully.")
    #     |> redirect(to: Routes.search_path(conn, :show, search))

    #   {:error, %Ecto.Changeset{} = changeset} ->
    #     render(conn, "new.html", changeset: changeset)
    # end
  end

  def show(_conn, %{"id" => _id}) do
    # search = Whatever.get_search!(id)
    # render(conn, "show.html", search: search)
  end

  def edit(_conn, %{"id" => _id}) do
    # search = Whatever.get_search!(id)
    # changeset = Whatever.change_search(search)
    # render(conn, "edit.html", search: search, changeset: changeset)
  end

  def update(_conn, %{"id" => _id, "search" => _search_params}) do
    # search = Whatever.get_search!(id)

    # case Whatever.update_search(search, search_params) do
    #   {:ok, search} ->
    #     conn
    #     |> put_flash(:info, "Search updated successfully.")
    #     |> redirect(to: Routes.search_path(conn, :show, search))

    #   {:error, %Ecto.Changeset{} = changeset} ->
    #     render(conn, "edit.html", search: search, changeset: changeset)
    # end
  end

  def delete(_conn, %{"id" => _id}) do
    # search = Whatever.get_search!(id)
    # {:ok, _search} = Whatever.delete_search(search)

    # conn
    # |> put_flash(:info, "Search deleted successfully.")
    # |> redirect(to: Routes.search_path(conn, :index))
  end
end
