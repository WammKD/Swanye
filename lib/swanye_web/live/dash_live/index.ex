defmodule SwanyeWeb.DashLive.Index do
  use   SwanyeWeb, :live_view;
  alias Kabare.Users;
  alias Swanye.SwanyeUsers.User;

  @impl true
  def mount(_params, session, socket) do
    {:ok, assign(socket, user: session["user"], page_num: session["page_num"])};
  end

  @impl true
  def handle_event("load-more-posts", _params, socket) do
    assigns   = socket.assigns;
    next_page = assigns.page_num + 1;
    user      = assigns.user;
    timeline  = user.timeline_posts;

    {:noreply,
     assign(socket,
            user:     %User{user | timeline_posts: timeline                                              ++
                                                   Users.get_timeline_posts_of_user(user.user_id,
                                                                                    List.last(timeline)
                                                                                        .activity_id)},
            page_num: next_page)};
  end
end
