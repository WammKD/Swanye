// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket,
// and connect at the socket path in "lib/web/endpoint.ex".
//
// Pass the token on params as below. Or remove it
// from the params if you are not using authentication.
import {Socket} from "phoenix"

let socket = null;

if(window.userToken               && window.userID               &&
   window.userToken !== null      && window.userID !== null      &&
   window.userToken !== undefined && window.userID !== undefined) {
  socket = new Socket("/socket", {params: {token: window.userToken}});

  // When you connect, you'll often need to authenticate the client.
  // For example, imagine you have an authentication plug, `MyAuth`,
  // which authenticates the session and assigns a `:current_user`.
  // If the current user exists you can assign the user's token in
  // the connection for use in the layout.
  //
  // In your "lib/web/router.ex":
  //
  //     pipeline :browser do
  //       ...
  //       plug MyAuth
  //       plug :put_user_token
  //     end
  //
  //     defp put_user_token(conn, _) do
  //       if current_user = conn.assigns[:current_user] do
  //         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
  //         assign(conn, :user_token, token)
  //       else
  //         conn
  //       end
  //     end
  //
  // Now you need to pass this token to JavaScript. You can do so
  // inside a script tag in "lib/web/templates/layout/app.html.eex":
  //
  //     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
  //
  // You will need to verify the user token in the "connect/3" function
  // in "lib/web/channels/user_socket.ex":
  //
  //     def connect(%{"token" => token}, socket, _connect_info) do
  //       # max_age: 1209600 is equivalent to two weeks in seconds
  //       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
  //         {:ok, user_id} ->
  //           {:ok, assign(socket, :user, user_id)}
  //         {:error, reason} ->
  //           :error
  //       end
  //     end
  //
  // Finally, connect to the socket:
  socket.connect();

  // Now that you are connected, you can join channels with a topic:
  /* let channel = socket.channel("topic:subtopic", {}) */
  let channel = socket.channel("room:notifications", {});

  channel.on("new_msg:" + window.userID,
             payload => {
               console.log(payload);

               if(payload.update_type === "notification") {
                 if(payload.add                           &&
                    window.Notification                   &&
                    Notification.permission === "granted") {
                   new Notification(payload.actors[0]                                  +
                                    (payload.type === "Like"                          ?
                                     " liked your post"                               :
                                     (payload.type === "Announce"                    ?
                                      " reblogged your post"                         :
                                      (payload.type === "Follow" ?
                                       " followed you"           : " doesn't know!")))/* ,
                                    { body: text, icon: "/images/missing.png" } */);
                 }


                 const count = document.getElementById("notifications_icon");

                 if(count && count !== null && count !== undefined) {
                   const notifications = parseInt(count.children[1].textContent, 10);

                   if(payload.add) {
                     if(notifications === 0) {
                       count.children[1].setAttribute("style", "");
                     }

                     count.children[1].textContent = notifications + 1;
                   } else {
                     if(notifications === 1) {
                       count.children[1].setAttribute("style", "display: none");
                     }

                     if(notifications > 0) {
                       count.children[1].textContent = notifications - 1;
                     }
                   }
                 }


                 const notifs = document.getElementById("notifications");

                 if(notifs) {
                   if(payload.add) {
                     const div  = document.createElement("div"),
                           span = document.createElement("span"),
                           hr   = document.createElement("hr");

                     span.appendChild(document.createTextNode("\n" + payload.actors[0]                           +
                                                              (payload.type === "Like"                          ?
                                                               " liked your post"                               :
                                                               (payload.type === "Announce"                    ?
                                                                " reblogged your post"                         :
                                                                (payload.type === "Follow" ?
                                                                 " followed you"           : " doesn't know!")))));

                     div.id = "notif_" + payload.id;
                     div.appendChild(Array.prototype
                                          .slice
                                          .call(document.getElementById("notification_icons")
                                                        .children)
                                          .find(function(el) {
                                                  return el.getAttribute("class")
                                                           .includes(payload.type === "Like"       ?
                                                                     "icon-gridicons-heart"        :
                                                                     (payload.type === "Announce" ?
                                                                      "icon-gridicons-reblog"     :
                                                                      (payload.type === "Follow" ?
                                                                       "icon-gridicons-follow"   :
                                                                       " doesn't know!")));
                                                })
                                          .cloneNode(true));
                     div.appendChild(span);
                     div.appendChild(hr);

                     notifs.children[0].parentNode.insertBefore(div,
                                                                notifs.children[0]);

                     let prevNotif = null;
                     for(let index in notifs.children) {
                       const notif = notifs.children[index];

                       if(notif               && notif         !== null  &&
                          notif !== undefined && notif.tagName === "DIV") {
                         if(prevNotif !== null                              &&
                            notif.getAttribute("style") === "display: none") {
                           prevNotif.setAttribute("style", "display: none");

                           break;
                         }

                         prevNotif = notif;
                       }
                     }
                   } else {
                     const notifToDelete = document.getElementById("notif_" + payload.id);

                     if(notifToDelete               &&
                        notifToDelete !== null      &&
                        notifToDelete !== undefined) {
                       const displayNewNotif = notifToDelete.getAttribute("style") !== "display: none";

                       notifToDelete.parentNode.removeChild(notifToDelete);

                       if(displayNewNotif) {
                         for(let index in notifs.children) {
                           const notif = notifs.children[index];

                           if(notif               && notif         !== null  &&
                              notif !== undefined && notif.tagName === "DIV") {
                             if(notif.getAttribute("style") === "display: none") {
                               notif.setAttribute("style", "");

                               break;
                             }
                           }
                         }
                       }
                     }
                   }
                 }
               } else {
                 const count = document.getElementById("home_icon");

                 if(count && count !== null && count !== undefined) {
                   const new_posts = parseInt(count.children[1].textContent, 10);

                   if(payload.add) {
                     if(new_posts === 0) {
                       count.children[1].setAttribute("style", "");
                     }

                     count.children[1].textContent = new_posts + 1;
                   } else {
                     if(new_posts === 1) {
                       count.children[1].setAttribute("style", "display: none");
                     }

                     if(new_posts > 0) {
                       count.children[1].textContent = new_posts - 1;
                     }
                   }
                 }
               }
             });

  channel.join()
         .receive("ok",    resp => { console.log("Joined successfully", resp); })
         .receive("error", resp => { console.log("Unable to join",      resp); });
}

export default socket
