# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :swanye,
  ecto_repos: [Swanye.Repo]

config :kabare,
  repo:                 Swanye.Repo,
  user:                 Swanye.SwanyeUsers.User,
  port:                 4000,
  scheme:               "http",
  hostname:             "localhost",
  on_activity_received: &Swanye.SwanyeActivityEffects.activity_effect/1

# Configures the endpoint
config :swanye, SwanyeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "d4SN/cxSBn2JaAoEOlUup/Q480Ub5K249yVtyPI+tZrySG6hXCpAppjLUJoCoB+U",
  render_errors: [view: SwanyeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Swanye.PubSub,
  live_view: [signing_salt: "MSh+QST6"]

# Configures the E-mail sender, Bamboo
config :swanye, Swanye.Mailer,
  adapter:      Bamboo.MailgunAdapter,
  api_key:      {:system, "MAILGUN_API_KEY"},
  domain:       {:system, "MAILGUN_DOMAIN"},
  hackney_opts: [recv_timeout: :timer.minutes(1)]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.29",
  default: [
    args:
      ~w(js/app.js js/socket.js js/easymde_2_16_1.min.js js/easy-markdown-editor.js --bundle --target=es2017 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Custome mime types
config :mime, :types, %{"application/activity+json" => "activity+json"}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
