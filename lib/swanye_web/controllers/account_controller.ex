defmodule SwanyeWeb.AccountController do
  use   SwanyeWeb, :controller;
  alias Swanye.Repo;
  alias Kabare.URLs.URL;
  alias Kabare.Utils.DatabaseUtils;
  alias Kabare.Users;
  alias Swanye.SwanyeUsers;
  alias Swanye.SwanyeUsers.User;
  alias Kabare.Utils.ActivityPubUtils;
  # alias Swanye.Settings
  # alias Swanye.Settings.Account

  @userStruct {%{}, %{header_image: :map,    profile_icon: :map,
                      display_name: :string, timezone:     :string, bio: :string}};
  defp validate_icon_size(changeset) do
    icon = Ecto.Changeset.get_field(changeset, :profile_icon);

    if(!is_nil(icon.path)) do
      {_mime, width, height, _type} = ExImageInfo.info(File.read!(icon.path));

      if(width === height) do
        changeset;
      else
        Ecto.Changeset.add_error(changeset,
                                 :profile_icon,
                                 "is not of equal height and width");
      end
    else
      changeset;
    end
  end
  defp validate_images_file_size(changeset) do
    icon   = Ecto.Changeset.get_field(changeset, :profile_icon);
    banner = Ecto.Changeset.get_field(changeset, :header_image);

    changeset |> (fn(chgset) ->
                    if(!is_nil(icon.path)) do
                      if(File.stat!(icon.path).size > 2 * 1024 * 1024) do
                        Ecto.Changeset.add_error(chgset,
                                                 :profile_icon,
                                                 "is greater than 2MiB");
                      else
                        chgset;
                      end
                    else
                      chgset;
                    end
                  end).()
              |> (fn(chgset) ->
                    if(!is_nil(banner.path)) do
                      if(File.stat!(banner.path).size > 2 * 1024 * 1024) do
                        Ecto.Changeset.add_error(chgset,
                                                 :header_image,
                                                 "is greater than 2MiB");
                      else
                        chgset;
                      end
                    else
                      chgset;
                    end
                  end).();
  end

  def settings(conn, _params) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user = Repo.preload(Users.get_full_user!(id),
                                 [:notifications,
                                  [actor: [object: [images: [object: [url: [:origin,
                                                                            :path]]]]]]]);

             render(conn,
                    "settings.html",
                    user:      user,
                    changeset: Ecto.Changeset.cast(@userStruct,
                                                   %{"header_image" => %Plug.Upload{},
                                                     "profile_icon" => %Plug.Upload{},
                                                     "display_name" => user.actor.object.name,
                                                     "timezone"     => user.timezone,
                                                     "bio"          => user.summary_as_markdown},
                                                   [:header_image, :profile_icon,
                                                    :display_name, :timezone,     :bio]),
                    page:      :settings);
    end
  end

  defp generate_image_map(newImage, usr, isIcon) do
    existingImages = Enum.filter(usr.actor.object.images,
                                 &(if isIcon, do: &1.is_icon, else: !&1.is_icon));

    if(is_binary(newImage.path)) do
      newImageBinary = File.read!(newImage.path);
      newPath        = "#{if isIcon, do: "icons", else: "banners"}/"                                                  <>
                       "#{usr.actor.preferred_username}_"                                                             <>
                       "#{Base.url_encode64(Calendar.strftime(DateTime.utc_now(), "%a, %d %b %Y %H:%M:%S GMT"))}.jpg";

      if(length(existingImages) > 0) do
        existingImageBinary = HTTPoison.get!(existingImages |> Enum.at(0)
                                                            |> DatabaseUtils.to_url_field()).body;

        if(existingImageBinary === newImageBinary) do
          %{"mediaType" => newImage.content_type,
	    "type"      => "Image",
	    "url"       => DatabaseUtils.to_url_field(Enum.at(existingImages, 0))};
        else
          %{"mediaType" => newImage.content_type,
	    "type"      => "Image",
	    "url"       => ("Swanye" |> ExAws.S3.put_object(newPath,
                                                            newImageBinary,
                                                            [acl: :public_read])
                                     |> ExAws.request!()).request_url};
        end
      else
        %{"mediaType" => newImage.content_type,
	  "type"      => "Image",
	  "url"       => ("Swanye" |> ExAws.S3.put_object(newPath,
                                                          newImageBinary,
                                                          [acl: :public_read])
                                   |> ExAws.request!()).request_url};
      end
    else
      if(length(existingImages) > 0) do
        %{"mediaType" => "image/jpeg",
	  "type"      => "Image",
	  "url"       => DatabaseUtils.to_url_field(Enum.at(existingImages, 0))}
      else
        nil
      end
    end
  end
  def save(conn, %{"update" => %{"header_image" => %Plug.Upload{} = banner,
                                 "profile_icon" => %Plug.Upload{} = icon,
                                 "display_name" => ""      <>       name,
                                 "bio"          => ""      <>       bio,
                                 "timezone"     => ""      <>       timezone}} = params) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user      = id |> Users.get_full_user!()
                            |> Repo.preload([actor: [[following: [[actor_items: [:inbox, :shared_inbox_endpoint]]]],
                                                     [followers: [[actor_items: [:inbox, :shared_inbox_endpoint]]]],
                                                     [object:    :images]]]);
             userID    = URL.to_string(user.actor.object.activity_pub_id);
             userAsMap = Jason.decode!(Jason.encode!(user));
             chgset    = @userStruct |> Ecto.Changeset.cast(params["update"],
                                                            [:header_image, :profile_icon,
                                                             :display_name, :timezone,     :bio])
                                     |> validate_icon_size()
                                     |> validate_images_file_size()
                                     |> User.is_valid_timezone?();

             if(chgset.valid?) do
               if(!Enum.any?(ExAws.request!(ExAws.S3.list_buckets()).body.buckets,
                             &(&1.name === "Swanye"))) do
                 ExAws.request!(ExAws.S3.put_bucket("Swanye",
                                                    ExAws.Config.new(:s3).region));
               end

               # We need to do this for these fields as the other fields will
               # get caught by the `inbox` via `send_json`
               SwanyeUsers.update_user_summary_and_timezone(user, bio, timezone);

               ActivityPubUtils.send_json(user,
                                          (user.actor.followers.actor_items ++
                                           user.actor.following.actor_items) |> Enum.map(&(case &1.shared_inbox_endpoint do
                                                                                             nil -> DatabaseUtils.to_ap_id(&1.inbox);
                                                                                             pnt -> URL.to_string(pnt);
                                                                                           end))
                                                                             |> Enum.uniq_by(&Function.identity/1),
                                          &(userID <> "/updates/" <> &1),
                                          %{"@context" => userAsMap["@context"],
                                            "actor"    => userID,
                                            "to"       => ["https://www.w3.org/ns/activitystreams#Public"],
                                            "type"     => "Update",
                                            "object"   => userAsMap |> Map.delete("@context")
                                                                    |> (fn(map) ->
                                                                          if(user.actor.object.name === name) do
                                                                            map;
                                                                          else
                                                                            Map.put(map, "name", name);
                                                                          end
                                                                        end).()
                                                                    |> (fn(map) ->
                                                                          bioHTML = Earmark.as_html!(bio);

                                                                          if(user.actor.object.summary === bioHTML) do
                                                                            map;
                                                                          else
                                                                            Map.put(map, "summary", bioHTML);
                                                                          end
                                                                        end).()
                                                                    |> (fn(map) ->
                                                                          Map.put(map,
                                                                                  "icon",
                                                                                  generate_image_map(icon,
                                                                                                     user,
                                                                                                     true));
                                                                        end).()
                                                                    |> (fn(map) ->
                                                                          Map.put(map,
                                                                                  "image",
                                                                                  generate_image_map(banner,
                                                                                                     user,
                                                                                                     false));
                                                                        end).()});
             end

             render(conn,
                    "settings.html",
                    user:      Repo.preload(Users.get_full_user!(id),
                                            [:notifications,
                                             [actor: [object: [images: [object: [url: [:origin,
                                                                                       :path]]]]]]]),
                    changeset: %{chgset | action: :update},
                    page:      :settings);
    end
  end
  def save(conn, %{"update" => %{"header_image" => %Plug.Upload{} = banner,
                                 "display_name" => ""      <>       name,
                                 "bio"          => ""      <>       bio,
                                 "timezone"     => ""      <>       timezone}}) do
    save(conn,
         %{"update" => %{"header_image" => banner, "profile_icon" => %Plug.Upload{},
                         "display_name" => name,   "timezone"     => timezone,       "bio" => bio}});
  end
  def save(conn, %{"update" => %{"profile_icon" => %Plug.Upload{} = icon,
                                 "display_name" => ""      <>       name,
                                 "bio"          => ""      <>       bio,
                                 "timezone"     => ""      <>       timezone}}) do
    save(conn,
         %{"update" => %{"header_image" => %Plug.Upload{}, "profile_icon" => icon,
                         "display_name" => name,           "timezone"     => timezone, "bio" => bio}});
  end
  def save(conn, %{"update" => %{"display_name" => "" <> name,
                                 "bio"          => "" <> bio,
                                 "timezone"     => "" <> timezone}}) do
    save(conn, %{"update" => %{"header_image" => %Plug.Upload{},
                               "profile_icon" => %Plug.Upload{},
                               "display_name" => name,
                               "bio"          => bio,
                               "timezone"     => timezone}});
  end

  def notifications(conn, _params) do
    case Plug.Conn.get_session(conn, "current_user") do
      nil -> Phoenix.Controller.redirect(conn,
                                         to: Routes.auth_path(conn,
                                                              :sign_in));
      id  -> user = Repo.preload(Users.get_full_user!(id),
                                 [notifications: [activity: [:object,
                                                             :acted_upon_object,
                                                             [actors: [object: [images: [object: [url: [:origin,
                                                                                                        :path]]]]]]]]]);

             Users.update_notifications_seen(user.notifications, true);

             render(conn,       "notifications.html",
                    user: user, page: :notifications);
    end
  end

  # def index(conn, _params) do
  #   accounts = Settings.list_accounts()
  #   render(conn, "index.html", accounts: accounts)
  # end

  # def new(conn, _params) do
  #   changeset = Settings.change_account(%Account{})
  #   render(conn, "new.html", changeset: changeset)
  # end

  # def create(conn, %{"account" => account_params}) do
  #   case Settings.create_account(account_params) do
  #     {:ok, account} ->
  #       conn
  #       |> put_flash(:info, "Account created successfully.")
  #       |> redirect(to: Routes.account_path(conn, :show, account))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "new.html", changeset: changeset)
  #   end
  # end

  # def show(conn, %{"id" => id}) do
  #   account = Settings.get_account!(id)
  #   render(conn, "show.html", account: account)
  # end

  # def edit(conn, %{"id" => id}) do
  #   account = Settings.get_account!(id)
  #   changeset = Settings.change_account(account)
  #   render(conn, "edit.html", account: account, changeset: changeset)
  # end

  # def update(conn, %{"id" => id, "account" => account_params}) do
  #   account = Settings.get_account!(id)

  #   case Settings.update_account(account, account_params) do
  #     {:ok, account} ->
  #       conn
  #       |> put_flash(:info, "Account updated successfully.")
  #       |> redirect(to: Routes.account_path(conn, :show, account))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", account: account, changeset: changeset)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   account = Settings.get_account!(id)
  #   {:ok, _account} = Settings.delete_account(account)

  #   conn
  #   |> put_flash(:info, "Account deleted successfully.")
  #   |> redirect(to: Routes.account_path(conn, :index))
  # end
end
