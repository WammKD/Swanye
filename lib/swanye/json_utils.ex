defmodule Swanye.JSONutils do
  @moduledoc """
  Functions to aid in dealing with JSON, such as modifying database structs to
  better be translated by the Jason module.
  """

  alias Kabare.URLs.URL;

  def cleanup_for_encoding(object) when is_binary(object) do
    object
  end
  def cleanup_for_encoding(object) when is_map(object)    do
    if(object["object"]) do
      Map.put(object,
              "object",
              replace_tags(object["object"]) |> replace_addressing( "to")
                                             |> replace_addressing("bto")
                                             |> replace_addressing( "cc")
                                             |> replace_addressing("bcc")) |> replace_addressing( "to")
                                                                           |> replace_addressing("bto")
                                                                           |> replace_addressing( "cc")
                                                                           |> replace_addressing("bcc");
    else
      replace_tags(object) |> replace_addressing( "to")
                           |> replace_addressing("bto")
                           |> replace_addressing( "cc")
                           |> replace_addressing("bcc");
    end
  end

  defp replace_tags(object) when is_binary(object) do
    object;
  end
  defp replace_tags(object) when is_map(object) do
    Map.put(object, "tag", case object["tag"] do
                             nil  -> nil;
                             tags -> Enum.map(tags,
                                              &(if(is_nil(&1["href"])) do
                                                  &1;
                                                else
                                                  Map.put(&1,
                                                          "href",
                                                          if(is_binary(&1["href"])) do
                                                            &1["href"];
                                                          else
                                                            URL.to_string(&1["href"].object.activity_pub_id);
                                                          end);
                                                end));
                           end);
  end

  defp replace_addressing(object, _addressType) when is_binary(object) do
    object;
  end
  defp replace_addressing(object,  addressType) when is_map(object)    do
    Map.put(object, addressType, case object[addressType] do
                                   nil        -> nil;
                                   addressees -> Enum.map(addressees,
                                                          &(if(is_binary(&1)) do
                                                              &1;
                                                            else
                                                              URL.to_string(&1.object.activity_pub_id);
                                                            end));
                                 end);
  end
end
